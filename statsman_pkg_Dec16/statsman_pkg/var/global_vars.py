# core dir
home_dir = '/opt/temp/statsman_refact/'
data_dir = '/var/statsman/checked/'
haystack = '/var/optimus/7.haystack'

# inner dir
var_dir = 'statsman_pkg/var/'
modules_dir = 'statsman_pkg/modules/'

# vector index 
# key : group
# value : interval start
vector_index = {
				'target_focus_mag_dmg':226,
				'target_focus_phis_dmg':233,
				'aoe_dmg_phis':240,
				'aoe_dmg_mag':247,
				'attack_dmg_passive':254,
				'reduce_armor_target':261,
				'reduce_armor_aoe':268,
				'increase_armor_self':275,
				'increase_armor_target':282,
				'increase_armor_aoe':289,
				'silence_target':296,
				'silence_aoe':303,
				'slow_target':310,
				'slow_aoe':317,
				'tp_stop':324,
				'tp_bkb_stop':331,
				'disable_target':338,
				'disable_aoe':345,
				'disable_bkb_unit':352,
				'mobility_hero':359,
				'reduce_inner_combo_dmg':366,
				'reduce_inner_mag_dmg':373,
				'reduce_enemy_phis_dmg':380,
				'increase_attack_dmg':387,
				'increase_mag_damage':394,
				'increase_combo_dmg':401,
				'healing_solo_unit':408,
				'healing_group_of_unit':415,
				'invis_self':422,
				'invise_teammate':429,
				'illusions_and_untits':436,
				'self_evassion':443,
				'debuf_evassion':450,
				'melee_attack':457,
				'range_attack':464,
				'combo_attack':471,
				'channelling_imba_skills':478,
				'drow_aura_abuse':485,
				'contr_melee':492,
				'contr_melee_bkb':499,
				'control_2sec':506,
				'control_ultra':513
				}