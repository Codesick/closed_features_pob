#!/usr/bin/env python
# cython: profile=True
import struct, gzip, zlib
import cProfile
from cpython cimport bool, int
from random import shuffle
cimport cython

dict_side_win_micro_fact = {}

@cython.boundscheck(False)
@cython.wraparound(False)
def vector_counting(list list_vector not None, dict dict_vector_check not None, bool mwin ):
	cdef:
		Py_ssize_t i, ii, iii, n

	n = len(list_vector)
	list_vector = sorted(list_vector)

	for i from 0 <= i < n:
		for ii from i+1 <= ii < n:
			for iii from ii+1 <= iii < n:
				try:
					dict_vector_check[(list_vector[i], list_vector[ii], list_vector[iii])][mwin] += 1
				except KeyError:
					dict_vector_check[(list_vector[i], list_vector[ii], list_vector[iii])] = [0,0]
					dict_vector_check[(list_vector[i], list_vector[ii], list_vector[iii])][mwin] = 1


# пары героев для никиты
@cython.boundscheck(False)
@cython.wraparound(False)
def vector_hero_counting(list hero_vector not None, dict dict_hero_vector_check not None, bool mwin ):
	cdef:
		Py_ssize_t i, ii, n

	n = len(hero_vector)
	hero_vector = sorted(hero_vector)

	for i from 0 <= i < n:
		for ii from i+1 <= ii < n:
			try:
				dict_hero_vector_check[(hero_vector[i], hero_vector[ii])][mwin] += 1
			except KeyError:
				dict_hero_vector_check[(hero_vector[i], hero_vector[ii])] = [0,0]
				dict_hero_vector_check[(hero_vector[i], hero_vector[ii])][mwin] = 1


def int_from_bytes(b):
	number, = struct.unpack(">i", b)
	return number

def int_from_compact(field):
	if not len(field): return 0
	return int(field)

def long_from_compact(field):
	if not len(field): return 0
	return int(field)

def long_from_bytes(b):
	number, = struct.unpack(">q", b)
	return number

def read_header(f):
	hmn_bytes = f.read(8)
	match_id_bytes = f.read(8)
	match_seq_num_bytes = f.read(8)
	end_time_bytes = f.read(4)
	key4_bytes = f.read(4)
	flags_bytes = f.read(4)
	size_bytes = f.read(4)
	fmn_bytes = f.read(8)
	if not hmn_bytes or not match_id_bytes or not match_seq_num_bytes or not end_time_bytes or not key4_bytes or not flags_bytes or not size_bytes or not fmn_bytes: return
	hmn_bytes = long_from_bytes(hmn_bytes)
	match_id = long_from_bytes(match_id_bytes)
	match_seq_num = long_from_bytes(match_seq_num_bytes)
	end_time = int_from_bytes(end_time_bytes)
	key4 = int_from_bytes(key4_bytes)
	flags = int_from_bytes(flags_bytes)
	size = int_from_bytes(size_bytes)
	#print '[size: {}]'.format(size)
	fmn_bytes = long_from_bytes(fmn_bytes)
	return (hmn_bytes, match_id, match_seq_num, end_time, key4, flags, size, fmn_bytes)

def read_data(f, size):
	compressed_data = f.read(size)
	data = zlib.decompress(compressed_data, zlib.MAX_WBITS|32)
	#print '[ratio: {}]'.format(float(len(data)) / len(compressed_data))
	return data

def read_block(f):
	header = read_header(f)
	if not header: return (None, None)
	data = read_data(f, header[6])
	return (header, data)

def match_from_compact(data):
	match = {}
	match_fields = data[1:-1].decode().split(',', 19)
	#match['end_time'] = int(match_fields[0])
	match['start_time'] = int_from_compact(match_fields[1])
	# поменял match['duration'] на long_from_compact
	match['duration'] = long_from_compact(match_fields[2])
	match['match_id'] = long_from_compact(match_fields[3])
	match['match_seq_num'] = long_from_compact(match_fields[4])
	match['radiant_win'] = match_fields[5] == "1"
	match['tower_status_radiant'] = int_from_compact(match_fields[6])
	match['tower_status_dire'] = int_from_compact(match_fields[7])
	match['barracks_status_radiant'] = int_from_compact(match_fields[8])
	match['barracks_status_dire'] = int_from_compact(match_fields[9])
	match['cluster'] = int_from_compact(match_fields[10])
	match['first_blood_time'] = int_from_compact(match_fields[11])
	match['lobby_type'] = int_from_compact(match_fields[12])
	match['human_players'] = int_from_compact(match_fields[13])
	match['leagueid'] = int_from_compact(match_fields[14])
	match['positive_votes'] = int_from_compact(match_fields[15])
	match['negative_votes'] = int_from_compact(match_fields[16])
	match['game_mode'] = int_from_compact(match_fields[17])
	match['engine'] = int_from_compact(match_fields[18])
	players = []
	if len(match_fields[19]) > 0:
		players_data = match_fields[19][1:-1].split(";")
		for player_data in players_data:
			player = player_from_compact(player_data)
			players.append(player)
	match['players'] = players
	return match

def player_from_compact(data):
	player = {}
	player_fields = data[1:-1].split(',', 23)
	player['account_id'] = int_from_compact(player_fields[0])
	player['player_slot'] = int_from_compact(player_fields[1])
	player['hero_id'] = int_from_compact(player_fields[2])
	player['item_0'] = int_from_compact(player_fields[3])
	player['item_1'] = int_from_compact(player_fields[4])
	player['item_2'] = int_from_compact(player_fields[5])
	player['item_3'] = int_from_compact(player_fields[6])
	player['item_4'] = int_from_compact(player_fields[7])
	player['item_5'] = int_from_compact(player_fields[8])
	player['kills'] = int_from_compact(player_fields[9])
	player['deaths'] = int_from_compact(player_fields[10])
	player['assists'] = int_from_compact(player_fields[11])
	player['leaver_status'] = int_from_compact(player_fields[12])
	player['gold'] = int_from_compact(player_fields[13])
	player['last_hits'] = int_from_compact(player_fields[14])
	player['denies'] = int_from_compact(player_fields[15])
	player['gold_per_min'] = int_from_compact(player_fields[16])
	player['xp_per_min'] = int_from_compact(player_fields[17])
	player['gold_spent'] = int_from_compact(player_fields[18])
	player['hero_damage'] = int_from_compact(player_fields[19])
	player['tower_damage'] = int_from_compact(player_fields[20])
	player['hero_healing'] = int_from_compact(player_fields[21])
	player['level'] = player_fields[22]
	ability_upgrades = []
	if len(player_fields[23]) > 0:
		#print_int(player_fields[23])
		ability_upgrades_data = player_fields[23][1:-1].split(":")
		for ability_upgrade_data in ability_upgrades_data:
			ability_upgrade = ability_upgrade_from_compact(ability_upgrade_data)
			ability_upgrades.append(ability_upgrade)
	player['ability_upgrades'] = player_fields[23]
	return player

def ability_upgrade_from_compact(data):
	ability_upgrade = {}
	ability_upgrade_fields = data[1:-1].split(',', 2)
	ability_upgrade['ability'] = int_from_compact(ability_upgrade_fields[0])
	ability_upgrade['time'] = int_from_compact(ability_upgrade_fields[1])
	ability_upgrade['level'] = int_from_compact(ability_upgrade_fields[2])
	return ability_upgrade

# ведем статистику побед / поражений на события из вектора матча
# есть подозрение, что написал для дебагинга и теперь не нужна
def micro_fact_change(index, side_win):
	try:
		dict_side_win_micro_fact[index][side_win] += 1
	except KeyError:
		dict_side_win_micro_fact[index] = [0,0]
		dict_side_win_micro_fact[index][side_win] = 1


### заполняем вектор матча

# проверяем количество героев заданой групы для rad и dire
# записываем индекс случая в вектор
def change_index(match, list_vector, hero_group, index):
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			if i < 5:
				if player['hero_id'] in hero_group:
					rad += 1
			else:
				if player['hero_id'] in hero_group:
					dire += 1
	key_r = index + rad
	key_d = index + 10000 + dire

	list_vector.append(key_r)
	list_vector.append(key_d)

	# micro_fact_change(key_r, match['radiant_win'])
	# micro_fact_change(key_d, match['radiant_win'])

# записываем в вектор героев из заданного матча 
def heroes_in_match(match, list_vector):
	cdef int i
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			if i < 5:
				key_r = 100 + player['hero_id']
				list_vector.append(key_r)
				micro_fact_change(key_r, match['radiant_win'])
			else:
				key_d = 10100 + player['hero_id']
				list_vector.append(key_d)
				micro_fact_change(key_d, match['radiant_win'])

# записываем в вектор продолжительность матча и инициализируем
# запись героев и групп
def vector_check(match, list_vector, heroes_groups):
	cdef int dur = round(match['duration']/300)
	if 0 < dur <= 30:
		list_vector.append(dur)
		micro_fact_change(dur, match['radiant_win'])
	elif dur > 30:
		list_vector.append(33)
		micro_fact_change(33, match['radiant_win'])

	heroes_in_match(match, list_vector)
	#  vector_index глобальная 
	for key in vector_index:
		change_index(match, list_vector, heroes_groups[key], vector_index[key])


### ищем известные события в новом матче
@cython.boundscheck(False)
@cython.wraparound(False)
def fact_cnt(list list_vector not None, dict dict_of_facts not None, cnt_fact_for_match):
	cnt = 0
	res = [0,0]
	unfiltred = {}
	match_fact_pool = []

	cdef:
		Py_ssize_t i, ii, iii, n

	n = len(list_vector)
	list_vector = sorted(list_vector)
	for i from 0 <= i < n:
		for ii from i+1 <= ii < n:
			for iii from ii+1 <= iii < n:
				try:
					unfiltred[(list_vector[i], list_vector[ii], list_vector[iii])] = dict_of_facts[(list_vector[i], list_vector[ii], list_vector[iii])]
				except KeyError:
					pass
	cnt = len(unfiltred)
	

	ranged_fact_list = filter_fact_modify(unfiltred)


	# количество фактов в выборке ranged_fact_list
	fact_list_len = 0
	for rang in ranged_fact_list:
		fact_list_len += len(rang[0])
	# print(fact_list_len)
	# устанавливаем ограничение на количество фактов турнира
	if fact_list_len > cnt_fact_for_match:
		stop = cnt_fact_for_match
	else:
		stop = fact_list_len


	# формируем список фактов для отправки во frontend
	rang = 0
	count = 0
	if ranged_fact_list != [[[],set()]]:
		while count < stop:
			for fact in ranged_fact_list[rang][0]:
				match_fact_pool.append(fact)
				count += 1
				if count == stop:
					break
			rang += 1

	# # check
	# print('match_fact_pool')
	# for fact in match_fact_pool:
	# 	print(fact)

	return match_fact_pool


def elements(fact):
	index_set = set()
	for index in fact:
		index_set.add(index)
	return index_set
	
def trash_check(fact, rang_set):
	res_i = 0
	for i in fact:
		if i in rang_set:
			res_i += 1
	return res_i

# попытка дописать к факту вин_рейт
# не проверенно, влияние на зависимости не оцененно
def filter_fact_modify(unfiltred):	
	# # check
	# capacity = 0 
	# print('filter_fact_modify start')
	# for key in unfiltred:
	# 	capacity += 1
	# print(capacity)

	unfiltred_list =[]
	ranged_fact_list = [[[],set()]]
	trash_query = []
	for key in unfiltred:
		unfiltred_list.append(key)
	shuffle(unfiltred_list)

	# for fact, side_win in unfiltred.items():
	for fact in unfiltred_list:
		index_set = elements(fact)
		for ii, rang in enumerate(ranged_fact_list):
			if rang[1].isdisjoint(index_set):
				ranged_fact_list[ii][0].append([fact, unfiltred[fact]])
				ranged_fact_list[ii][1] = rang[1].union(index_set)
				# #check
				# print('new fact - ', rang[0][-1])
				break
			elif trash_check(fact, rang[1]) > 1:
				trash_query.append([fact, unfiltred[fact]])
				break
			elif ii == len(ranged_fact_list) - 1:
				ranged_fact_list.append([[[fact, unfiltred[fact]]], index_set])
				# #check
				# print('new rang - ', ranged_fact_list[-1][0])
				break
	# check
	# print('filter_fact_modify stop')
	return ranged_fact_list


def vector_pooling(list_vector, dict_sign_mwin, mwin):
	for sign in list_vector:
		try:
			dict_sign_mwin[sign][mwin] += 1
		except KeyError:
			dict_sign_mwin[sign] = [0,0]
			dict_sign_mwin[sign][mwin] = 1

# тестовая надстройка для подсчета винрейтов пар героев 
def double_hero_fact(match, hero_vector):
	heroes_in_match(match, hero_vector)

def read_haystack_main(filename, retantion, heroes_name, heroes_groups, dict_of_facts, cnt_fact_for_match):
	# for check
	last_match = False
	dict_vector_check = {}
	dict_hero_vector_check = {}
	dict_sign_mwin = {}
	max_facts = [0,0]
	cdef int cheked_mtch_cnt = 0
	cdef int counter = 0
	from collections import Counter
	with open(filename, "rb") as f:
		offset = 0
		while (counter < retantion):
			#неверная логика, поправить
			counter += 1
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)
			if counter > 1000001:
				match = match_from_compact(data)
				if match['duration'] > 599:
					cheked_mtch_cnt += 1 
					list_vector = [0, 10001]

					vector_check(match, list_vector, heroes_groups)

					vector_pooling(list_vector, dict_sign_mwin, match['radiant_win'])
					# vector_counting(list_vector, dict_vector_check, match['radiant_win'] )
					haystack_fact_pool.update({match['match_id']:fact_cnt(list_vector, dict_of_facts, cnt_fact_for_match)})

					# # пары героев для NIKITA
					# hero_vector = []
					# double_hero_fact(match, hero_vector)
					# vector_hero_counting(hero_vector, dict_hero_vector_check, match['radiant_win'] )

					


				if counter % (10**4) == 0:
					print()
					print('counter: %d. cheked_mtch_cnt: %d' % (counter, cheked_mtch_cnt))
			elif counter % (10**5*2) == 0:
				print('counter: %d' % counter)
	print()
	import pickle
	with open('/var/statsman/cheked/haystack_fact_pool_unreadable.pickle', 'wb') as f:
		pickle.dump( haystack_fact_pool, f)
	# винрейты признаков вектора
	# with open('/var/statsman/cheked/9m_dict_sign_mwin.pickle', 'wb') as f:
	# 	pickle.dump( dict_sign_mwin, f)
	# # Для Никиты
	# with open('/var/statsman/cheked/9m_dict_hero_vector_check.pickle', 'wb') as f:
	# 	pickle.dump( dict_hero_vector_check, f)
	# словарь всех возможных фактов
	# with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_vector_check.pickle', 'wb') as f:
	# 	pickle.dump( dict_vector_check, f)
	print('buybuy')

	# with open('/var/statsman/cheked/' + str(retantion // 10**6*2) + 'kk_match_facts_cnt.pickle', 'wb') as f:
	# 	pickle.dump( match_facts_cnt, f)

	# with open('/var/statsman/cheked/dict_side_win_micro_fact.pickle', 'wb') as f:
	# 	pickle.dump( dict_side_win_micro_fact, f)

def main(heroes_name, heroes_groups, dict_of_facts, vect_index):
	print('main_v2.0')
	global vector_index
	vector_index = vect_index
	global haystack_fact_pool
	haystack_fact_pool = {}

	#########################################################################################################################################################
	# heroes_name	- словарь имена и индексы героев согласно dota2 api
	# dict_of_facts - словарь интересных фактов: ключ - (x,y,z) - факт, значение [a,b] - a : rad_win; b: dire_win
	# heroes_groups - словарь групп героев: ключ - 'name_of_group', значение - [a1,a2,...,an] - индекс героя(как в dota2 api)
	# vector_index	- словарь с индексами начала диапозона группы: index = 0 героев из данной группы в команде, index+1 = 1 геройданной группы в команде и тд
	# retantion		- порядковый номер турнира в хайстеке, по достижении, программа остановится
	# cnt_fact_for_match - нормативное количество интересных фактов на матч
	retantion = 1000100
	cnt_fact_for_match = 10
	read_haystack_main('/var/optimus/7.haystack', retantion, heroes_name, heroes_groups, dict_of_facts, cnt_fact_for_match)

	# cProfile.runctx("read_haystack_main('/var/optimus/7.haystack', 1001001, heroes_name, heroes_groups, dict_of_facts, vector_index )", globals() ,locals())

# if __name__ == "__main__":
# 	main()
