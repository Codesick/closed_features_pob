from random import shuffle


def trash_check(fact, ranged_fact_list):
	res_i = 0
	for i in fact:
		for rang in ranged_fact_list:

			if i in rang[1]:
				res_i += 1
				break
	return res_i

def filter_fact(unfiltred_list):	
	shuffle(unfiltred_list)
	print(unfiltred_list)
	ranged_fact_list = [[[],set()]]
	trash_query = []
	for i, fact in enumerate(unfiltred_list):
		index_set = elements(fact)
		for ii, rang in enumerate(ranged_fact_list):
			if rang[1].isdisjoint(index_set):
				print('rang_',rang)
				print('fact_', fact)
				rang[0].append(fact)
				rang[1] = rang[1].union(index_set)
				# for indx in index_set:
				# 	rang[1].add(indx)
				break
			elif trash_check(fact, ranged_fact_list) > 1:
				trash_query.append(fact)
				break
			elif ii == len(ranged_fact_list) - 1:
				ranged_fact_list.append([[fact], index_set])
				break

	print('trash:', trash_query)
	print()
	return ranged_fact_list

def elements(fact):
	index_set = set()
	for index in fact:
		index_set.add(index)
	return index_set


def main():
	a = (1, 3, 6)
	b = (21, 22, 23)
	c = (1, 7, 10)
	d = (2, 8, 13)
	e = (5, 15, 25)
	mas = [(2,5,7)]
	mas.append(a)
	mas.append(b)
	mas.append(c)
	mas.append(d)
	mas.append(e)
	
	sup = [[[],set()]]
	mas.append((9, 11, 19))
	ranged = filter_fact(mas)
	for i,it in enumerate(ranged):
		print('rang_'+ str(i+1) + ': facts:', ranged[i])
		print()

if __name__ == "__main__":
	main()