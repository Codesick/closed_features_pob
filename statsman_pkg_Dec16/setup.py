# команда bash:
# python3 setup.py build_ext --inplace
from distutils.core import setup
from Cython.Build import cythonize
from statsman_pkg.var.global_vars import home_dir
setup(
	ext_modules=cythonize("%sstatsman_pkg/modules/statsman_cython.pyx" % (home_dir))
)