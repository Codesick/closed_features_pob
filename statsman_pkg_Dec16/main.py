#!/usr/bin/env python
import pickle, json

from statsman_pkg.modules import statsman_cython
from statsman_pkg.var.global_vars import home_dir, var_dir, vector_index

def load_json_file(file_name):
	
	inp_file = '%s%s%s' % (home_dir, var_dir, file_name)

	#inp_file = '/opt/statsman_refact/statsman_pkg/var/' + file_name
	return inp_file

def main():
	print('main')
	with open(load_json_file("heroes.json")) as heroes_json:
		heroes_name = json.load(heroes_json)['heroes']
	print('heroes')
	with open(load_json_file("heroes_groups.json")) as heroes_groups_json:
		heroes_groups = json.load(heroes_groups_json)['groups']
		print('heroes_groups')
	with open('/opt/statsman/final_ifacts_dict.pickle', 'rb') as f:
		dict_of_facts = pickle.load(f)


	# #check
	# gr_fact = 7 
	# for i, fact in enumerate(dict_of_facts):
	# 	print(fact, end = ' ')
	# 	if i == 20:
	# 		break
	# command 1
	statsman_cython.main(
							heroes_name, heroes_groups, dict_of_facts, vector_index
						)

if __name__ == "__main__":
	main()
