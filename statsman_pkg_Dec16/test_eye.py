#!/usr/bin/env python
from statsman_pkg.modules import test_r
import pickle

def side_win_readable(rad_dire):
	if rad_dire[0] >= rad_dire[1]:
		return 'radiant_win_%d' % (rad_dire[0]*100//(rad_dire[1] + rad_dire[0]))
	else:
		return 'dire_win_%d' % (rad_dire[1]*100//(rad_dire[1] + rad_dire[0]))

def get_names():
	from statsman_pkg.var.fact_notation import notes
	with open('/var/statsman/cheked/haystack_fact_pool_unreadable.pickle', 'rb') as f:
		haystack_fact_pool = pickle.load(f)
	# # check
	# print(haystack_fact_pool)
	
	readable_haystack_fact_pool = {}
	for key, value in haystack_fact_pool.items():
		readable_match_facts =[]

		# # check
		# print('key - ',key)
		# print('value - ', value)
		for fact in value:
			readable_fact = []
			for index in fact[0]:
				readable_fact.append(notes[index])
			readable_fact.append(side_win_readable(fact[1]))
			readable_match_facts.append(readable_fact)
		readable_haystack_fact_pool.update({key:readable_match_facts})
	# print(readable_haystack_fact_pool[2323190786])	
	return readable_haystack_fact_pool

def make_readable_file(readable_haystack_fact_pool):

	res_text = ''
	for key, value in readable_haystack_fact_pool.items():
		# print(key)
		res_text += str(key) + '\n'
		for fact in value:
			# print(fact)
			res_text += str(fact) + '\n'
		res_text += '\n'
	with open('n100matches_wth_10facts.txt','w') as f:
		f.write(res_text)

		# break

def main():
	# test_r.main()
	readable_haystack_fact_pool = get_names()
	make_readable_file(readable_haystack_fact_pool)

	########################
	######## TEST ZONE #####
	########################
	with open('/opt/statsman/dict_of_facts.pickle', 'rb') as f:
		dict_of_facts = pickle.load(f)
	# # check ## too much long & and unstructable
	# print(dict_of_facts)


if __name__ == "__main__":
	main()
