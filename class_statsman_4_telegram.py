# справка
# ключи heroname_histogramm
# ['duration']
# ['kills']
# ['deaths']
# ['gold_per_min']
# ['hero_damage']
# ['hero_healing']

# block hero stats analize				
class Statsman:

  def __init__(self):
    self.keys = ['duration', 'kills', 'deaths', 'gold_per_min', 'hero_damage', 'hero_healing']
    import json
    with open(self.load_json_file("./heroes.json")) as heroes_json:
      self.heroes_name = json.load(heroes_json)['heroes']
    self.hists = self.open_file()
    # for hero in self.heroes_name:
    #   try:
    #     self.hists[hero['id']] = self.open_file(hero['id'])
    #   except FileNotFoundError:
    #     print('FileNotFoundError', hero['id'])

  def load_json_file(self, file_name):
    home_dir = '/opt/bot/main/'
    var_dir = ''
    inp_file = '%s%s%s' % (home_dir, var_dir, file_name)
    return inp_file

  def open_file(self):
    import pickle
    # print('wait_pls') 
    # for hero in self.heroes_name:
    #   if hero['id'] == hero_id:
    #     file_name = hero['localized_name']
    # with open('/opt/bot/heroes_hist/' + file_name + '.pickle', 'rb') as f:
    #   hist_full = pickle.load(f)
    with open('/var/telegram/telegram_dict.pickle', 'rb') as f:
      hist_full = pickle.load(f)
    return hist_full

  def sub_count(self, hist, real_val):
    # ключи hist
    # 0 - значения в диапозоне, 1 - граничное значение диапозона
    # находим первую границу диапозона, которая больше значения игрока
    for i, val in enumerate(hist[1]):
      if val > real_val:
        save_i = i - 1
        # print('val', val)
        break
    part = 0
    full_hist = 0
    # считаем количество игр в истории, в которых значение атрибута было меньше чем у игрока
    # считаем общее количество игр
    for i, val in enumerate(hist[0]):
      full_hist += hist[0][i]
      if i <= save_i:
        part += hist[0][i]
    # print(part, full_hist)
    partition = part*100/(full_hist)
    return partition

  def partition_count(self, hist_full, player_value):
    # тоже что снизу, для ключа ['duration'], но читаемое
    # partitions['duration'] = sub_count(hist_full['duration'], player_value['duration'])

    # для каждого ключа "keys", ищем сколько процентов поля он обошел по ключевому показателю
    partitions = {}
    for key in self.keys:
      partitions[key] = self.sub_count(hist_full[key], player_value[key])
    return partitions

  def create_player_value(self, match, player_id = None):
    player_value = None
    if player_id is not None:
      for player in match['players']:
        if player['account_id'] ==  player_id:
          player_value = [{}]
          player_value[0]['duration'] = match['duration']
          player_value[0]['account_id'] = player['account_id']
          player_value[0]['hero_id'] = player['hero_id']
          for key in self.keys:
            if key != 'duration':
              player_value[0][key] = player[key]
    else:
      player_value = []
      cnt = 0
      for player in match['players']:
        player_value.append({})
        player_value[cnt]['duration'] = match['duration']
        player_value[cnt]['account_id'] = player['account_id']
        player_value[cnt]['hero_id'] = player['hero_id']
        for key in self.keys:
          if key != 'duration':
            player_value[cnt][key] = player[key]
        cnt += 1
    return player_value

  def stats_participate(self, match, player_id = None):
    # match = {'duration':2400, 'players':[{'kills':20, 'deaths':0, 'gold_per_min':850, 'hero_damage':30500, 'hero_healing': 1000, 'hero_id': 1, 'account_id': 12105}, {'kills':10, 'deaths':10, 'gold_per_min':550, 'hero_damage':17500, 'hero_healing': 1000, 'hero_id': 1, 'account_id': 13105}]}
    res = {}
    player_value = self.create_player_value(match, player_id)
    if player_value is not None:
      for pl_value in player_value:
        try:
          histogramm = self.hists[pl_value['hero_id']]
        except KeyError:
          return res
        partitions = self.partition_count(histogramm, pl_value)
        for key in self.keys:
          res.update({ key: {'hero_name': pl_value['hero_id'], 'value': pl_value[key], 'partition': partitions[key]}})
          # print('id: %d - partition %s for %d : %f' % (pl_value['account_id'], key, pl_value[key], partitions[key]))
    else:
      print('player id is not in this game.')
    return res

def hero_id_to_hero_name(heroes, hero_id):
  for hero in heroes:
    if hero['id'] == hero_id:
      return hero['localized_name']
      