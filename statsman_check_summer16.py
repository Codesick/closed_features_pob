#!/usr/bin/env python
import ast
import struct, gzip, zlib
import pickle, json, os
from glob import iglob
import time
import copy
import cProfile

winlose = {'win':0, 'lose':0}
vector_index = {
				'target_focus_mag_dmg':226,
				'target_focus_phis_dmg':233,
				'aoe_dmg_phis':240,
				'aoe_dmg_mag':247,
				'attack_dmg_passive':254,
				'reduce_armor_target':261,
				'reduce_armor_aoe':268,
				'increase_armor_self':275,
				'increase_armor_target':282,
				'increase_armor_aoe':289,
				'silence_target':296,
				'silence_aoe':303,
				'slow_target':310,
				'slow_aoe':317,
				'tp_stop':324,
				'tp_bkb_stop':331,
				'disable_target':338,
				'disable_aoe':345,
				'disable_bkb_unit':352,
				'mobility_hero':359,
				'reduce_inner_combo_dmg':366,
				'reduce_inner_mag_dmg':373,
				'reduce_enemy_phis_dmg':380,
				'increase_attack_dmg':387,
				'increase_mag_damage':394,
				'increase_combo_dmg':401,
				'healing_solo_unit':408,
				'healing_group_of_unit':415,
				'invis_self':422,
				'invise_teammate':429,
				'illusions_and_untits':436,
				'self_evassion':443,
				'debuf_evassion':450,
				'melee_attack':457,
				'range_attack':464,
				'combo_attack':471,
				'channelling_imba_skills':478,
				'drow_aura_abuse':485,
				'contr_melee':492,
				'contr_melee_bkb':499,
				'control_2sec':506,
				'control_ultra':513
				}

def hhw_update(dictionary, fact_notation):
	hero1 = fact_notation[1]['hero1']
	hero2 = fact_notation[1]['hero2']
	hh_cnt = fact_notation[1]['|h1 <<with>> h2|']
	win_cnt = fact_notation[1]['|win|']

	if hero1 in dictionary:
		dictionary[hero1].update({
			hero2 : { 'hh_estimate': hh_cnt,
					  'win_estimate': win_cnt}
			})
	else:
		dictionary.update({ 
			hero1 : { hero2 : { 'hh_estimate': hh_cnt,
								'win_estimate': win_cnt}}
			})

def h_update(dictionary, fact_notation):
	hero = fact_notation[1]['hero']
	h_cnt = fact_notation[1]['|hero|']

	dictionary[hero] = {'estimate': h_cnt}

def hwi_update( dictionary, fact_notation):
	hero = fact_notation[1]['hero']
	item = fact_notation[1]['item']
	cnt = fact_notation[1]['|hero <<with>> item|']
	cnt_hero = fact_notation[1]['|hero|']
	if hero in dictionary:
		dictionary[hero].update({
			item : { 'hwi_estimate': cnt}
			})
	else:
		dictionary.update({ 
			hero : { item : { 'hwi_estimate': cnt}}
			})
	if hero in dictionary:
		dictionary[hero].update({ 
			'h_estimate' : cnt_hero
			})
	else:
		dictionary.update({ 
			hero : { 'h_estimate' : cnt_hero}
			})

def hwiVSh_update(dictionary, fact_notation):
	hero = fact_notation[1]['hero']
	item = fact_notation[1]['item']
	enemy = fact_notation[1]['enemy']
	cnt = fact_notation[1]['|hero <<with>> item <<vs>> enemy|']
	if hero in dictionary:
		if item in dictionary[hero]:
			dictionary[hero][item].update({
				enemy: {
					'esteamate': cnt,
					}
				})
		else:
			dictionary[hero].update({
				item: {
					enemy: {
						'esteamate': cnt
						}
					}
				})
	else:
		dictionary.update({
			hero: {
				item: {
					enemy: {
						'esteamate': cnt
						}
					}
				}
			})

def finded_to_dict(file, function):
	facts = []
	res = {}
	with open(file, 'r') as f:
		for line in f:
			facts.append(ast.literal_eval( line))
	for fact in facts:
		fact_notation = fact[next(iter(fact))]
		function(dictionary = res, fact_notation = fact_notation)
	return res

def cnt_from_db(name, db, topic):
	try:
		return list(db.query('select count(value) from ' +  topic + ' where id=\'' + str(name) + '\''))[0][0]['count']
	except IndexError:
		return 0

def int_from_bytes(b):
	number, = struct.unpack(">i", b)
	return number

def int_from_compact(field):
	if not len(field): return 0
	return int(field)

def long_from_compact(field):
	if not len(field): return 0
	return int(field)

def long_from_bytes(b):
	number, = struct.unpack(">q", b)
	return number

def read_header(f):
	hmn_bytes = f.read(8)
	match_id_bytes = f.read(8)
	match_seq_num_bytes = f.read(8)
	end_time_bytes = f.read(4)
	key4_bytes = f.read(4)
	flags_bytes = f.read(4)
	size_bytes = f.read(4)
	fmn_bytes = f.read(8)
	if not hmn_bytes or not match_id_bytes or not match_seq_num_bytes or not end_time_bytes or not key4_bytes or not flags_bytes or not size_bytes or not fmn_bytes: return
	hmn_bytes = long_from_bytes(hmn_bytes)
	match_id = long_from_bytes(match_id_bytes)
	match_seq_num = long_from_bytes(match_seq_num_bytes)
	end_time = int_from_bytes(end_time_bytes)
	key4 = int_from_bytes(key4_bytes)
	flags = int_from_bytes(flags_bytes)
	size = int_from_bytes(size_bytes)
	#print '[size: {}]'.format(size)
	fmn_bytes = long_from_bytes(fmn_bytes)
	return (hmn_bytes, match_id, match_seq_num, end_time, key4, flags, size, fmn_bytes)

def read_data(f, size):
	compressed_data = f.read(size)
	data = zlib.decompress(compressed_data, zlib.MAX_WBITS|32)
	#print '[ratio: {}]'.format(float(len(data)) / len(compressed_data))
	return data

def read_block(f):
	header = read_header(f)
	if not header: return (None, None)
	data = read_data(f, header[6])
	return (header, data)

def match_from_compact(data):
	match = {}
	match_fields = data[1:-1].decode().split(',', 19)
	#match['end_time'] = int(match_fields[0])
	match['start_time'] = int_from_compact(match_fields[1])
	# поменял match['duration'] на long_from_compact
	match['duration'] = long_from_compact(match_fields[2])
	match['match_id'] = long_from_compact(match_fields[3])
	match['match_seq_num'] = long_from_compact(match_fields[4])
	match['radiant_win'] = match_fields[5] == "1"
	match['tower_status_radiant'] = int_from_compact(match_fields[6])
	match['tower_status_dire'] = int_from_compact(match_fields[7])
	match['barracks_status_radiant'] = int_from_compact(match_fields[8])
	match['barracks_status_dire'] = int_from_compact(match_fields[9])
	match['cluster'] = int_from_compact(match_fields[10])
	match['first_blood_time'] = int_from_compact(match_fields[11])
	match['lobby_type'] = int_from_compact(match_fields[12])
	match['human_players'] = int_from_compact(match_fields[13])
	match['leagueid'] = int_from_compact(match_fields[14])
	match['positive_votes'] = int_from_compact(match_fields[15])
	match['negative_votes'] = int_from_compact(match_fields[16])
	match['game_mode'] = int_from_compact(match_fields[17])
	match['engine'] = int_from_compact(match_fields[18])
	players = []
	if len(match_fields[19]) > 0:
		players_data = match_fields[19][1:-1].split(";")
		for player_data in players_data:
			player = player_from_compact(player_data)
			players.append(player)
	match['players'] = players
	return match

def player_from_compact(data):
	player = {}
	player_fields = data[1:-1].split(',', 23)
	player['account_id'] = int_from_compact(player_fields[0])
	player['player_slot'] = int_from_compact(player_fields[1])
	player['hero_id'] = int_from_compact(player_fields[2])
	player['item_0'] = int_from_compact(player_fields[3])
	player['item_1'] = int_from_compact(player_fields[4])
	player['item_2'] = int_from_compact(player_fields[5])
	player['item_3'] = int_from_compact(player_fields[6])
	player['item_4'] = int_from_compact(player_fields[7])
	player['item_5'] = int_from_compact(player_fields[8])
	player['kills'] = int_from_compact(player_fields[9])
	player['deaths'] = int_from_compact(player_fields[10])
	player['assists'] = int_from_compact(player_fields[11])
	player['leaver_status'] = int_from_compact(player_fields[12])
	player['gold'] = int_from_compact(player_fields[13])
	player['last_hits'] = int_from_compact(player_fields[14])
	player['denies'] = int_from_compact(player_fields[15])
	player['gold_per_min'] = int_from_compact(player_fields[16])
	player['xp_per_min'] = int_from_compact(player_fields[17])
	player['gold_spent'] = int_from_compact(player_fields[18])
	player['hero_damage'] = int_from_compact(player_fields[19])
	player['tower_damage'] = int_from_compact(player_fields[20])
	player['hero_healing'] = int_from_compact(player_fields[21])
	player['level'] = player_fields[22]
	ability_upgrades = []
	if len(player_fields[23]) > 0:
		#print_int(player_fields[23])
		ability_upgrades_data = player_fields[23][1:-1].split(":")
		for ability_upgrade_data in ability_upgrades_data:
			ability_upgrade = ability_upgrade_from_compact(ability_upgrade_data)
			ability_upgrades.append(ability_upgrade)
	player['ability_upgrades'] = player_fields[23]
	return player

def ability_upgrade_from_compact(data):
	ability_upgrade = {}
	ability_upgrade_fields = data[1:-1].split(',', 2)
	ability_upgrade['ability'] = int_from_compact(ability_upgrade_fields[0])
	ability_upgrade['time'] = int_from_compact(ability_upgrade_fields[1])
	ability_upgrade['level'] = int_from_compact(ability_upgrade_fields[2])
	return ability_upgrade



def read_haystack_1(filename):
	counter = 0
	start_time = time.time()
	from collections import Counter

	with open(filename, "rb") as f:
		offset = 0
		dazzle = 0
		heroes_cnt = Counter()
		heroes_cnt_w_d = Counter()
		while (counter < 1000000):
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)

			match = match_from_compact(data)
			counter += 1
			flag = False
			for player in match['players']:
				if player['hero_id'] == 50:
					flag = True

			for player in match['players']:
				if flag == True:
					dazzle += 1
					if player['hero_id'] != 50:
						heroes_cnt_w_d[player['hero_id']] += 1
				else:
					if player['hero_id'] != 50:
						heroes_cnt[player['hero_id']] += 1
	print(dazzle)
	for hero in heroes_cnt:
		if hero != 0:
			print(hero, float(heroes_cnt_w_d[hero])*100/heroes_cnt[hero])


def read_haystack_hhw(filename, hhw, retantion):
	start_time = time.time()
	counter = 0
	from collections import Counter

	with open(filename, "rb") as f:
		offset = 0
		dazzle = 0
		heroes_cnt = Counter()
		heroes_cnt_w_d = Counter()
		match_cnt = 0
		matches_win = 0
		while (counter < retantion):
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)

			match = match_from_compact(data)
			counter += 1

			for i, player in enumerate(match['players']):
				if player['hero_id'] in hhw :
					for j , allies in enumerate(match['players']):
						if (allies['hero_id'] in hhw[player['hero_id']] and j < 5 and i < 5 ) :
							try:
								hhw[player['hero_id']][allies['hero_id']]['real_hh_cnt'] += 1
							except KeyError:
								hhw[player['hero_id']][allies['hero_id']]['real_hh_cnt'] = 1
							if match['radiant_win'] == 0:
								try:
									hhw[player['hero_id']][allies['hero_id']]['real_win_cnt'] += 1
								except KeyError:
									hhw[player['hero_id']][allies['hero_id']]['real_win_cnt'] = 1
						elif (allies['hero_id'] in hhw[player['hero_id']] and j > 4 and i > 4 ):
							try:
								hhw[player['hero_id']][allies['hero_id']]['real_hh_cnt'] += 1
							except KeyError:
								hhw[player['hero_id']][allies['hero_id']]['real_hh_cnt'] = 1
							if match['radiant_win'] == 1:
								try:
									hhw[player['hero_id']][allies['hero_id']]['real_win_cnt'] += 1
								except KeyError:
									hhw[player['hero_id']][allies['hero_id']]['real_win_cnt'] = 1

				print("Run time: {:.3f} min |||".format(( time.time() - start_time)/60 ), "1kk time: {:.3f} min".format(( time.time() - start_time)/60*(1000000/counter)), "ids: ", counter, end='\r')
	print()
	with open('/var/statsman/cheked/1kk_hhw.pickle', 'wb') as f:
		pickle.dump( hhw, f)


def read_haystack_hero(filename, h, retantion):
	start_time = time.time()
	counter = 0

	with open(filename, "rb") as f:
		offset = 0
		while (counter < retantion):
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)

			match = match_from_compact(data)
			counter += 1
			for player in match['players']:
				if player['hero_id'] in h:
					try:
						h[player['hero_id']]['real'] += 1
					except KeyError:
						h[player['hero_id']]['real'] = 1
			print("Run time: {:.3f} min |||".format(( time.time() - start_time)/60 ), "1kk time: {:.3f} min".format(( time.time() - start_time)/60*(1000000/counter)), "ids: ", counter, end='\r')
	print()
	for player in h:
		if abs(h[player]['estimate'] - h[player]['real'])/float(h[player]['real']) - 1 > 0.05:
			print(h[player])

def load_json_file(file_name):
    inp_file = os.path.abspath(os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "..",
        "statsman",
        file_name))
    return inp_file


def read_haystack_mem_check(filename, retantion, heroes_name, interesting_items ):
	from pympler import asizeof

	start_time = time.time()
	counter = 0
	dict_inject = {'match_cnt': 0, 'win_match_cnt':0, 'allies_items': {}}
	hwivsh1 = dict_init(heroes_name, dict_inject)
	from collections import Counter

	with open(filename, "rb") as f:
		offset = 0
		dazzle = 0
		heroes_cnt = Counter()
		heroes_cnt_w_d = Counter()
		match_cnt = 0
		matches_win = 0
		while (counter < retantion):
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)

			match = match_from_compact(data)
			counter += 1
			for i, player in enumerate(match['players']):
				for ii, allies in enumerate(match['players']):
					if i < 5 and ii > 4 and match['radiant_win'] == 1 and player['hero_id'] != 0 and allies['hero_id'] != 0:
						try:
							hwivsh1[player['hero_id']][allies['hero_id']]['win_match_cnt'] += 1
						except	KeyError:
							pass	
						for j in range(0,6):
							item = 'item_'+str(j)
							if player[item] in interesting_items:
								try:
									hwivsh1[player['hero_id']][allies['hero_id']][player[item]] += 1
								except KeyError:
									hwivsh1[player['hero_id']][allies['hero_id']][player[item]] = 1
								#немного порнографии, для теста памяти
								for jj in range(0,6):
									item_al = 'item_'+str(jj)
									if player[item_al] in interesting_items:
										try:
											hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]] += 1
										except KeyError:
											hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]]= 1

					elif i > 4 and ii < 5 and match['radiant_win'] == 0 and player['hero_id'] != 0 and allies['hero_id'] != 0:
						try:
							hwivsh1[player['hero_id']][allies['hero_id']]['win_match_cnt'] += 1
						except KeyError:
							pass
						for j in range(0,6):
							item = 'item_'+str(j)
							if player[item] in interesting_items:
								try:
									hwivsh1[player['hero_id']][allies['hero_id']][player[item]] += 1
								except KeyError:
									hwivsh1[player['hero_id']][allies['hero_id']][player[item]] = 1
								#немного порнографии, для теста памяти
								for jj in range(0,6):
									item_al = 'item_'+str(jj)
									if player[item_al] in interesting_items:
										try:
											hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]] += 1
										except KeyError:
											hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]]= 1


			print("Run time: {:.3f} min |||".format(( time.time() - start_time)/60 ), "1kk time: {:.3f} min".format(( time.time() - start_time)/60*(1000000/counter)), "ids: ", counter, end='\r')
			if counter % 10**5 == 0:
				print()
				print(asizeof.asizeof(hwivsh1) / float(1024*1024))
	print()
	with open('/var/statsman/cheked/1kk_hwivsh1.pickle', 'wb') as f:
		pickle.dump( hwivsh1, f)

def dict_init(heroes_name, dict_inject):
	#добавить возможность создания шаблона без dict_inject

	res_dict = {}
	inject = copy.deepcopy(dict_inject)
	for i, name1 in enumerate(heroes_name):
		res_dict[name1['id']] = {}
	for i, name1 in enumerate(heroes_name):
		for ii, name2 in enumerate(heroes_name):
			res_dict[name1['id']][name2['id']] = inject
	return  res_dict
	
# надо протестить и пере писать все check под dict_init_v2
# возможно нужно разделить dict_inject на 
# dict_inject_hero
# dict_inject_teammate, dict_inject_allies
# диаграмма с структурой словаря - dict_stracture template.html
def dict_init_v2(heroes_name, dict_inject):
	res_dict = {}
	inject = copy.deepcopy(dict_inject)
	for i, name1 in enumerate(heroes_name):
		res_dict[name1['id']] = {'teammate':{}, 'allies':{}}
		res_dict[name1['id']].update(inject)
	for i, name1 in enumerate(heroes_name):
		for ii, name2 in enumerate(heroes_name):
			res_dict[name1['id']]['teammate'][name2['id']] = inject
			res_dict[name1['id']]['allies'][name2['id']] = inject
	return res_dict
		
# словарь вещей
def dict_items_init(interesting_items, dict_inject):
	res_dict = {}
	for item in interesting_items:
		res_dict[item] = dict_inject
	return res_dict
# словарь винрейтов для команд из 4х и 5ти ближников
def dict_melee_team_init():
	res_dict = {}
	for i in range(0,6):
		key = 'melee_' + str(i)
		res_dict.update({key:winlose.copy()})
	return res_dict

# drows teammetes attack type
def dict_drow_wth_range_init():
	res_dict = {}
	for i in range(1,6):
		key = 'range_' + str(i)
		res_dict.update({key:winlose.copy()})
	return res_dict

# словарь винрейтов для команды из 3, 4 , 5  
# героев со  способностями по увеличению физ урона
def dict_increse_dmg_team_init():
	res_dict = {}
	for i in range(3,6):
		key = 'boost_x' + str(i)
		res_dict.update({key:winlose.copy()})
	return res_dict

# словарь винрейтов для команд с хиллером и без
def dict_team_wthout_heal_init():
	res_dict = {}
	res_dict.update({'wth_heal':winlose.copy(), 'both_teams_wth_heal':0} )
	return res_dict 

# словарь колличество героев с дизейблами в команде
def dict_disable_heroes_init():
	res_dict = {}
	for i in range(0,6):
		key = 'dis_h_' + str(i)
		res_dict.update({key:winlose.copy()})
	return res_dict

# колличество героев с уклонением в команде
def dict_evassion_heroes_init():
	res_dict = {}
	for i in range(0,6):
		key = 'ev_h_' + str(i)
		res_dict.update({key:winlose.copy()})
	return res_dict

# героев в команде 1 с armor_reduce против 
# героев в команде 2 с armor_reduce
def dict_reduce_armor_team_init():
	res_dict = {}
	for i in range(0,6):
		for ii in range(0,6):
			key = str(i) + 'vs' + str(ii)
			res_dict.update({key:winlose.copy()})
	return res_dict


# правило 
# килы деленые на ассисты одной команды в сравнении с 
# другой командой, если выйграла команда у которой 
# это отношение больше то  true, нет - false
def dict_team_fight_rule_init():
	res_dict = {}
	res_dict.update({'true':winlose.copy(), 'false':winlose.copy()} )
	return res_dict 

# вектор матча
def dict_vector_init():
	res_dict = {}
	for i in range(1,600):
		key = i
		res_dict.update({key:False})

	for i in range(10101,10600):
		key = i
		res_dict.update({key:False})
	res_dict[0] = True
	res_dict[10100] = True
	return res_dict

def dict_vector_check_init(dict_vector):
	start_time = time.time()
	counter = 0
	res_dict = {}
	for key_1 in dict_vector:
		for key_2 in dict_vector:
			if key_2 > key_1:
				for key_3 in dict_vector:
					if key_3 > key_2:
						counter += 1
						new_key = str(key_1) + '.' + str(key_2) + '.' + str(key_3)
						res_dict[new_key] = [0,0]
						print("Run time: {:.3f} min |||".format(( time.time() - start_time)/60 ), "1kk time: {:.3f} min".format(( time.time() - start_time)/60*(222000000/counter)), "ids: ", counter, ' 111', end='\r')	
	return res_dict
	
	
# проверка на то, что 2 игрока в составе одной команды
def r_u_teammate(i, ii):
	if i == ii:
		return 'rule_error'
	else:
		if  i < 5 and ii < 5:
			return True
		elif i > 4 and ii > 4:
			return True
		else:
			return False
# проверка на то, что 2 игрока в составе разных команд
def r_u_allies(i, ii):
	if i == ii:
		return 'rule_error'
	else:
		if  i < 5 and ii > 4:
			return True
		elif i > 4 and ii < 5:
			return True
		else:
			return False

#проверка игрока на то, что он в команде победителей
def winner(i, match):
	if i < 5 and match['radiant_win'] == 1:
		return True
	elif i > 4 and match['radiant_win'] == 0:
		return True
	else:
		return False



# герой собрал предмет, играя против героя и при этом выйграл
# на входе требуется подготовленный dict
def check1(match, hwivsh1 , heroes_name, interesting_items):
	for i, player in enumerate(match['players']):
		for ii, allies in enumerate(match['players']):
			if i < 5 and ii > 4 and match['radiant_win'] == 1 and player['hero_id'] != 0 and allies['hero_id'] != 0:
				hwivsh1[player['hero_id']][allies['hero_id']]['win_match_cnt'] += 1
				for j in range(0,6):
					item = 'item_'+str(j)
					if player[item] in interesting_items:
						try:
							hwivsh1[player['hero_id']][allies['hero_id']][player[item]] += 1
						except KeyError:
							hwivsh1[player['hero_id']][allies['hero_id']][player[item]] = 1
						#немного порнографии, для теста памяти
						for jj in range(0,6):
							item_al = 'item_'+str(jj)
							if player[item_al] in interesting_items:
								try:
									hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]] += 1
								except KeyError:
									hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]]= 1

			elif i > 4 and ii < 5 and match['radiant_win'] == 0 and player['hero_id'] != 0 and allies['hero_id'] != 0:
				hwivsh1[player['hero_id']][allies['hero_id']]['win_match_cnt'] += 1
				for j in range(0,6):
					item = 'item_'+str(j)
					if player[item] in interesting_items:
						try:
							hwivsh1[player['hero_id']][allies['hero_id']][player[item]] += 1
						except KeyError:
							hwivsh1[player['hero_id']][allies['hero_id']][player[item]] = 1
						#немного порнографии, для теста памяти
						for jj in range(0,6):
							item_al = 'item_'+str(jj)
							if player[item_al] in interesting_items:
								try:
									hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]] += 1
								except KeyError:
									hwivsh1[player['hero_id']][allies['hero_id']]['allies_items'][allies[item_al]]= 1
	return  hwivsh1

# 2 игрока одной команды собрали один и тот же предмет
# 
def check2(match, dict_check2, interesting_items):
	for i, player in enumerate(match['players']):
		for ii, teammate in enumerate(match['players']):
			if r_u_teammate(i, ii):
				for j in range(0,6):
					item_pl = 'item_'+str(j)
					if player[item_pl] in interesting_items:
						for jj in range(0,6):
							item_te = 'item_'+str(j)
							if player[item_pl] == teammate[item_te]:
								dict_check2[player['hero_id']][teammate['hero_id']]['match_cnt'] += 1
								if winner(i, match):
									dict_check2[player['hero_id']][teammate['hero_id']]['win_game'][player[item_pl]] += 1
								elif winner(i, match) == False:
									dict_check2[player['hero_id']][teammate['hero_id']]['lose_game'][player[item_pl]] += 1


# добавляем доп ключи к dict_check3
def check3_dict_update(dict_check3, heroes_name):
	for i, name1 in enumerate(heroes_name):
		dict_check3[name1['id']].update(winlose.copy())
		for ii, name2 in enumerate(heroes_name):
			dict_check3[name1['id']][name2['id']].update(winlose.copy())
	return  dict_check3

# win rate hero1 versus hero2
# {{{fail}}}
# не будет работать, так как нельзя посчитать матчи hero1 с условием,
# что против него не играл allies
# диаграмма с структурой словаря - check3.html
def check3(match, dict_check3, heroes_name):
	check3_dict_update(dict_check3, heroes_name)
	for i, player in enumerate(match['players']):
		# этот кусок вносит ошибку, так как данная победа/поражение
		# значима только для игр в которых player не играет против 5 
		# allies из текущего матча
		if player['hero_id'] != 0:
			if winner(i, match):
				dict_check3[player['hero_id']]['win'] += 1
			elif winner(i, match) == False:
				dict_check3[player['hero_id']]['lose'] += 1
			for ii, allies in enumerate(match['players']):
				if r_u_allies(i, ii) and allies['hero_id'] != 0:
					if winner(i, match):
						dict_check3[player['hero_id']][allies['hero_id']]['win'] += 1
					elif winner(i, match) == False:
						dict_check3[player['hero_id']][allies['hero_id']]['lose'] += 1



### ДЛЯ БЛОКА EASY CHECK
### dict_easy_init нужно написать
### возможно есть смысл использовать один словарь на все 
### easy_checks, тогда нужно  переделать ключ 'match_cnt'
### чтобы появился идентификатор связи 
### check_n_parametr с счетчиком матчей

# каунтеры win/lose матчей по герою
# нужен dict_easy_check1 init
def easy_check1(match, dict_easy_check):
	for i, player in enumerate(match['players']):
		if winner(i,match) and player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['win'] += 1
		elif player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['lose'] += 1

# средний gpm по герою
# нужен dict_easy_check2 init
def easy_check2(match, dict_easy_check):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:				
			dict_easy_check[player['hero_id']]['gpm_cnt'] += player['gold_per_min']
			dict_easy_check[player['hero_id']]['match_gpm_cnt'] += 1


# средний xpm по герою
# нужен dict_easy_check3 init
def easy_check3(match, dict_easy_check):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['xpm_cnt'] += player['xp_per_min']
			dict_easy_check[player['hero_id']]['match_xpm_cnt'] += 1

# средние убийства у героя
def easy_check4(match, dict_easy_check):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['kills'] += player['kills']
			dict_easy_check[player['hero_id']]['match_kills_cnt'] += 1	

# средние смерти у героя
def easy_check5(match, dict_easy_check):
	for i, player in enumerate(match['players']): 
		if player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['deaths'] += player['deaths']
			dict_easy_check[player['hero_id']]['match_deaths_cnt'] += 1
# средние ассисты у героя
def easy_check6(match, dict_easy_check):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			dict_easy_check[player['hero_id']]['assists'] += player['assists']
			dict_easy_check[player['hero_id']]['match_assists_cnt'] += 1


# экономим строки кода в hero_value_place
def dict_check4_edit(dict_check4, side_sorted, mwin):
	for i, hero in enumerate(side_sorted):
		if  mwin and hero[0] != 0:
			dict_check4[hero[0]][str(i + 1)]['win'] += 1
			# можно добавить данные для подсчета средней стоимости
			# в конце игры
			#dict_check4[hero[0]][str( i + 1)]['win_summary_gold'] += hero[i + 1]
		elif hero['hero_id'] != 0:
			dict_check4[hero[0]][str(i + 1)]['lose'] += 1
			# можно добавить данные для подсчета средней стоимости
			# в конце игры
			#dict_check4[hero[0]][str( i + 1)]['lose_summary_gold'] += hero[i + 1]


# для каждого героя находим его место в команде
# 1-5 в зависимости от общей ценности
# делаем каунтеры: сколько выйграл, сколько проиграл на данной позиции
# диаграмма с структурой словаря - check4.html
#hero_value_place
def check4(match, dict_check4):
	rad = {}
	dire = {}
	mwin = match['radiant_win']
	for i, player in enumerate(match['players']):
		if i < 5:
			rad[player['hero_id']] = player['gold']
		else:
			dire[player['hero_id']] = player['gold']

	rad_sorted = sorted(rad, key=rad.get, reverse=True)
	dire_sorted = sorted(dire, key=dire.get, reverse=True)
	#dict_check4_edit(dict_check4, rad_sorted, mwin)
	#dict_check4_edit(dict_check4, dire_sorted, not mwin)
	

# каунтеры на герой плюс вещь
def check5(match, dict_check5 , interesting_items):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			for j in range(0,6):
				item = 'item_'+str(j)
				if (player[item] in interesting_items) and winner(i, match):
					dict_check5[player['hero_id']][player[item]]['win'] += 1
				elif player[item] in interesting_items:
					dict_check5[player['hero_id']][player[item]]['lose'] += 1


# каунтеры на герой плюс вещь_1 и вещь_2
def check6(match, dict_check6 ,  interesting_items):
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			for j in range(0,6):
				item1 = 'item_'+str(j)
				if player[item1] in interesting_items:
					for jj in range(0,6):
						item2 = 'item_'+str(jj)
						if player[item2] in interesting_items:
							if  j != jj and winner(i, match):
								dict_check6[player['hero_id']][player[item1]][player[item2]]['win'] += 1
							elif j != jj:
								dict_check6[player['hero_id']][player[item1]][player[item2]]['lose'] += 1		


# check7_data_calculate
# dict_check7_update
def check7_calc(items, key, dict_check7):
	items.sort()
	dup_items = []
	for i, item in enumerate(items):
	    if i != 0:
	        if item == f_item:
	            dup_items.append(item)
	        else:
	            f_item = item
	    else:
	    	f_item = item
	s = set (dup_items)        
	for item in s:
	    dict_check7[item][key] += 1




# каунтеры на 2 одинаковых предмета в команде
# сократить список interesting_items
# оставить только модификаторы атаки и ауры
def check7(match, dict_check7 ,  interesting_items):
	items_rad = []
	items_dire = []
	mwin = match['radiant_win']
	for i, player in enumerate(match['players']):
		for j in range(0,6):
			item = 'item_'+str(j)
			# interesting_items стоит заметить на другой список
			#  (ауры + модификаторы атак)
			if player[item] in interesting_items:
				if i < 5:
					items_rad.append(player[item])
				elif i > 4:
					items_dire.append(player[item])
	if mwin:
		rad_key = 'win'
		dire_key = 'lose'
	else:
		rad_key = 'lose'
		dire_key = 'win'
	check7_calc(items_rad, rad_key, dict_check7)
	check7_calc(items_dire, dire_key, dict_check7)

# win/lose кfунтеры для Hero1, для Hero1 + Hero2 в одной команде
def check8(match, dict_check8):
	mwin = match['radiant_win']
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			if mwin:
				dict_check8[player['hero_id']]['win'] += 1
			else:
				dict_check8[player['hero_id']]['lose'] += 1
			for ii, teammate in enumerate(match['players']):
				if r_u_teammate(i, ii) and teammate['hero_id'] != 0:
					if winner(i, match):
						dict_check8[player['hero_id']]['teammate'][teammate['hero_id']]['win'] += 1
					else:
						dict_check8[player['hero_id']]['teammate'][teammate['hero_id']]['lose'] += 1


# duration каунтеры для Hero
def check9(match, dict_check9):
	mwin = match['radiant_win']
	d = round(match['duration']/60)
	if d > 240:
		duration = str(240)
	else:
		duration = str(d)
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			if mwin:
				dict_check9[player['hero_id']]['win'] += 1
				dict_check9[player['hero_id']][duration]['win'] += 1
			else:
				dict_check9[player['hero_id']]['lose'] += 1
				dict_check9[player['hero_id']][duration]['lose'] += 1


# hero_damage каунтеры с шагом в 500
def check10(match, dict_check10):
	mwin = match['radiant_win']
	for i, player in enumerate(match['players']):
		if player['hero_id'] != 0:
			d = round(player['hero_damage']/500)
			if d > 300:
				hero_dmg = str(300)
			else:
				hero_dmg = str(d)
			if mwin:
				dict_check10[player['hero_id']]['win'] += 1
				dict_check10[player['hero_id']][hero_dmg]['win'] += 1
			else:
				dict_check10[player['hero_id']]['lose'] += 1
				dict_check10[player['hero_id']][hero_dmg]['lose'] += 1

# каунтеры на 4 и 5 милишников в команде
def melee_team_check(match, dict_melee_team, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if player['hero_id'] in heroes_groups['melee_attack']:
				rad += 1
		else:
			if player['hero_id'] in heroes_groups['melee_attack']:
				dire += 1

	# radiant
	key_r = 'melee_' + str(rad) 
	if mwin:
		dict_melee_team[key_r]['win'] += 1
	else:
		dict_melee_team[key_r]['lose'] += 1

	# dire
	key_d = 'melee_' + str(dire) 
	if not mwin:
		dict_melee_team[key_d]['win'] += 1
	else:
		dict_melee_team[key_d]['lose'] += 1

# каунтеры на количество ренджевиков в команде при наличии дровки
def drow_wth_range_check(match, dict_drow_wth_range, heroes_groups):
	mwin = match['radiant_win']
	for i, player in enumerate(match['players']):
		if player['hero_id'] == 6:
			ranges = 0
			for ii, drow_assister in enumerate(match['players']):
				if r_u_teammate(i, ii):
					if drow_assister['hero_id'] in heroes_groups['range_attack']:
						ranges += 1
			if winner(i, match):
				dict_drow_wth_range['range_' + str(ranges)]['win'] += 1
			else:
				dict_drow_wth_range['range_' + str(ranges)]['lose'] += 1
			break

# каунтеры на количество героев со способностями к увеличению физ урона 
def increase_dmg_team_check(match, dict_increse_dmg_team, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if player['hero_id'] in heroes_groups['increase_attack_dmg']:
				rad += 1
		else:
			if player['hero_id'] in heroes_groups['increase_attack_dmg']:
				dire += 1
	if rad > 2:
		if mwin:
			dict_increse_dmg_team['boost_x' + str(rad)]['win'] += 1
		else:
			dict_increse_dmg_team['boost_x' + str(rad)]['lose'] += 1
	if dire > 2:
		if mwin:
			dict_increse_dmg_team['boost_x' + str(dire)]['lose'] += 1
		else:
			dict_increse_dmg_team['boost_x' + str(dire)]['win'] += 1
			
# каунтеры на винрейт команды с хилером и без
def team_wthout_heal_check(match, dict_team_wthout_heal, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if (player['hero_id'] in heroes_groups['healing_solo_unit']) or (player['hero_id'] in heroes_groups['healing_group_of_unit']):
				rad += 1
		else:
			if (player['hero_id'] in heroes_groups['healing_solo_unit']) or (player['hero_id'] in heroes_groups['healing_group_of_unit']):
				dire += 1
	if rad > 0 and dire > 0:
		dict_team_wthout_heal['both_teams_wth_heal'] += 1
	elif rad > 0 and mwin:
		dict_team_wthout_heal['wth_heal']['win'] += 1
	elif rad > 0:
		dict_team_wthout_heal['wth_heal']['lose'] += 1
	elif dire > 0 and mwin:
		dict_team_wthout_heal['wth_heal']['lose'] += 1
	elif dire > 0:
		dict_team_wthout_heal['wth_heal']['win'] += 1

# каунтеры побед, по числe героев с дизейблом в команде
# считаем ситуации когда в одной команде 0 героев с дизейблом, а в другой > 2
def disable_heroes_in_team_check(match, dict_disable_heroes, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if (player['hero_id'] in heroes_groups['disable_target']) or (player['hero_id'] in heroes_groups['disable_aoe']):
				rad += 1
		else:
			if (player['hero_id'] in heroes_groups['disable_target']) or (player['hero_id'] in heroes_groups['disable_aoe']):
				dire += 1
	if rad == 0 and dire > 1:
		key = 'dis_h_' + str(dire)
		if mwin:
			dict_disable_heroes[key]['lose'] += 1
		else:
			dict_disable_heroes[key]['win'] += 1
	elif dire == 0 and rad > 1:
		key = 'dis_h_' + str(rad)
		if mwin:
			dict_disable_heroes[key]['win'] += 1
		else:
			dict_disable_heroes[key]['lose'] += 1


# каунтеры по количеству героев с уклонением в команде
def evassion_heroes_check(match, dict_evassion_heroes, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if (player['hero_id'] in heroes_groups['self_evassion']) or (player['hero_id'] in heroes_groups['debuf_evassion']):
				rad += 1
		else:
			if (player['hero_id'] in heroes_groups['self_evassion']) or (player['hero_id'] in heroes_groups['debuf_evassion']):
				dire += 1
	key_r = 'ev_h_' + str(rad)
	key_d = 'ev_h_' + str(dire)
	if mwin:
		dict_evassion_heroes[key_r]['win'] += 1
		dict_evassion_heroes[key_d]['lose'] += 1
	else:
		dict_evassion_heroes[key_r]['lose'] += 1
		dict_evassion_heroes[key_d]['win'] += 1

# heroes_rad vs heroes_dire 
# win - победа света
# lose - победа тьмы
# нужно переделать заполнение словаря, так как 0vs2 = 2vs0 и тп
def reduce_armor_team_check(match, dict_reduce_armor_team, heroes_groups):
	mwin = match['radiant_win']
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if (player['hero_id'] in heroes_groups['reduce_armor_target']) or (player['hero_id'] in heroes_groups['reduce_armor_aoe']):
				rad += 1
		else:
			if (player['hero_id'] in heroes_groups['reduce_armor_target']) or (player['hero_id'] in heroes_groups['reduce_armor_aoe']):
				dire += 1
	key = str(rad) + 'vs' + str(dire)
	if mwin:
		dict_reduce_armor_team[key]['win'] += 1
	else:
		dict_reduce_armor_team[key]['lose'] += 1	
#
def team_fight_rule_check(match, dict_team_fight_rule, heroes_groups):
	mwin = match['radiant_win']
	rad_assists = 0
	rad_kills = 0 
	dire_assists = 0
	dire_kills = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			rad_kills += int(player['hero_id']['kills'])
			rad_assists += player['hero_id']['assists']
		else:
			dire_kills += player['hero_id']['kills']
			dire_assists += player['hero_id']['assists']
	if mvin:
		if rad_kills/rad_assists > dire_kills/dire_assists:
			dict_team_fight_rule['true'] += 1
		else:
			dict_team_fight_rule['false'] += 1
	else:
		if rad_kills/rad_assists > dire_kills/dire_assists:
			dict_team_fight_rule['false'] += 1
		else:
			dict_team_fight_rule['true'] += 1

# заполняем вектор матча
def change_index(match, dict_vector, heroes_group, index):
	rad = 0
	dire = 0
	for i, player in enumerate(match['players']):
		if i < 5:
			if player['hero_id'] in heroes_group:
				rad += 1
		else:
			if player['hero_id'] in heroes_group:
				dire += 1
	key_r = index + rad
	key_d = index + 10000 + dire

	dict_vector[key_r] = True
	dict_vector[key_d] = True

	
def heroes_in_match(match, dict_vector):
	for i, player in enumerate(match['players']):
		if i < 5:
			key_r = 100 + player['hero_id']
			dict_vector[key_r] = True
		else:
			key_d = 10100 + player['hero_id']
			dict_vector[key_d] = True

def vector_check(match, dict_vector, heroes_groups):
	dur = round(match['duration']/300)
	if 0 < dur <= 30:
		dict_vector[dur] = True
	elif dur > 30:
		dict_vector[33] = True

	heroes_in_match(match, dict_vector)

	for key in vector_index:
		change_index(match, dict_vector, heroes_groups[key], vector_index[key])
	
def vector_minimize(dict_vector):
	res_list = []
	for key in dict_vector:
		if dict_vector[key]:
			res_list.append(key)
	return res_list

def vector_counting(list_vector_minimized, dict_vector_check, mwin ):
	for key_1 in list_vector_minimized:
		for key_2 in list_vector_minimized:
			if key_2 > key_1:
				for key_3 in list_vector_minimized:
					if key_3 > key_2:
						new_key = str(key_1) + '.' + str(key_2) + '.' + str(key_3)
						if new_key not in dict_vector_check:
							dict_vector_check[new_key] = [0,0]
						dict_vector_check[new_key][mwin] += 1
						


def read_haystack_main(filename, retantion, heroes_name, heroes_groups, interesting_items ):
	from pympler import asizeof

	start_time = time.time()
	counter = 0
###dict_create
	"""
	# вложеный словарь для check1, может быть проблема с названием словаря
	dict_inject_1 = {'match_cnt': 0, 'win_match_cnt':0, 'allies_items': {}}
	hwivsh1 = copy.deepcopy(dict_init(heroes_name, dict_inject_1))
	# init dict of check2
	int_items = {}
	for it in interesting_items:
		#s = it
		int_items.update({it:0})
	# словарь с каунтерами для каждого предмета
	dict_inject_2 = {'match_cnt': 0, 'win_game': int_items, 'lose_game': int_items}
	dict_check2 = copy.deepcopy(dict_init(heroes_name, dict_inject_2))

	# init dict of check3
	dict_inject_3 = {}
	dict_check3 = copy.deepcopy(dict_init(heroes_name, dict_inject_3))

	# init dict of check4 (manual)
	dict_check4 = {}
	for i, name1 in enumerate(heroes_name):
		dict_check4[name1['id']] ={}
		for ii in range(1,6):
			dict_check4[name1['id']][str(ii)] = winlose.copy()
	#init dict of check5

	dict_check5 = {}
	for i, name in enumerate(heroes_name):
		dict_check5[name['id']] ={}
		for j in interesting_items:
			dict_check5[name['id']][j] = winlose.copy()

	# init dict of check6
	dict_check6 = {}
	for i, name in enumerate(heroes_name):
		dict_check6[name['id']] ={}
		for j in interesting_items:
			dict_check6[name['id']][j] = {}
			for jj in interesting_items:
				dict_check6[name['id']][j][jj] = winlose.copy()

	# для easy_check иниты не писал
	dict_easy_inject = {'win':0, 'lose':0,
						 'gpm_cnt':0, 'match_gpm_cnt':0, 
						 'xpm_cnt':0, 'match_xpm_cnt':0, 
						 'kills':0, 'match_kills_cnt':0, 
						 'deaths':0,'match_deaths_cnt':0, 
						 'assists':0, 'match_assists_cnt':0
						 }
	dict_easy_check = {}
	for i, name1 in enumerate(heroes_name):
		dict_easy_check[name1['id']] = dict_easy_inject

	# init dict of check7
	dict_check7 = copy.deepcopy(dict_items_init(interesting_items, winlose.copy()))
	
	# init dict of check8
	dict_check8 = copy.deepcopy(dict_init_v2(heroes_name, winlose.copy()))

	# init dict of check9
	dict_check9 = {}
	time_dict = winlose.copy()
	for i in range(0,241):
		time_dict.update({str(i):winlose.copy()})
	for i, name in enumerate(heroes_name):
		dict_check9[name['id']] = time_dict

	# init dict of check10
	dict_check10 = {}
	dmg_dict = winlose.copy()
	for i in range(0,301):
		dmg_dict.update({str(i):winlose.copy()})
	for i, name in enumerate(heroes_name):
		dict_check10[name['id']] = dmg_dict

	# customs dict inits
	dict_melee_team = dict_melee_team_init()
	dict_drow_wth_range = dict_drow_wth_range_init() 
	dict_increse_dmg_team = dict_increse_dmg_team_init()
	dict_team_wthout_heal = dict_team_wthout_heal_init()
	dict_disable_heroes = dict_disable_heroes_init()
	dict_evassion_heroes = dict_evassion_heroes_init()
	dict_reduce_armor_team = dict_reduce_armor_team_init()
	#dict_mobi_vs_hard_control = dict_mobi_vs_hard_control()
	#dict_mobi_vs_silence = dict_mobi_vs_silence()
	dict_team_fight_rule = dict_team_fight_rule_init()
	
	###инициация словаря для записи данных из векторов турнира
	dict_vector = dict_vector_init()
	dict_vector_check = ctry.dict_vector_check_init(dict_vector)
	del(dict_vector)
	print('init done')
	# жрет память
	###print(asizeof.asizeof(dict_vector_check) / float(1024*1024))
	with open('/var/statsman/' + 'dict_vector_check_template.pickle', 'wb') as f:
		pickle.dump( dict_vector_check, f, protocol = 4)
	print('dump done')
	"""
	#with open('/var/statsman/' + 'dict_vector_check_template.pickle', 'rb') as f:
	#	dict_vector_check = pickle.load(f)
	dict_vector_check = {}
	"""
	dict_pool = {
				'dict_easy_check':dict_easy_check,
				'hwivsh1':hwivsh1,
				'dict_check2':dict_check2,
				'dict_check3':dict_check3,
				'dict_check4':dict_check4,
				'dict_check5':dict_check5,
				'dict_check6':dict_check6,
				'dict_check7':dict_check7,
				'dict_check8':dict_check8,
				'dict_check9':dict_check9,
				'dict_check10':dict_check10,
				'dict_melee_team':dict_melee_team,
				'dict_drow_wth_range':dict_drow_wth_range,
				'dict_increse_dmg_team':dict_increse_dmg_team,
				'dict_team_wthout_heal':dict_team_wthout_heal,
				'dict_disable_heroes':dict_disable_heroes,
				'dict_evassion_heroes':dict_evassion_heroes,
				'dict_reduce_armor_team':dict_reduce_armor_team,
				'dict_team_fight_rule':dict_team_fight_rule
				}			
	"""
### checks_start		
	from collections import Counter
	with open(filename, "rb") as f:
		from ctry import vector_counting as ctry_vector_counting
		offset = 0
		cheked_mtch_cnt = 0
		counter = 0
		while (counter < 2000):
			header, data = read_block(f)
			if header is None or data is None: break
			padding = 0
			if header[6] % 8 > 0:
				padding = 8 - header[6] % 8
			offset = offset + 48 + header[6] + padding
			f.seek(padding, 1)

			match = match_from_compact(data)
			counter += 1
			if match['duration'] > 599:
				cheked_mtch_cnt += 1 
				"""
				easy_check1(match, dict_easy_check)
				easy_check2(match, dict_easy_check)
				easy_check3(match, dict_easy_check)
				easy_check4(match, dict_easy_check)
				easy_check5(match, dict_easy_check)
				easy_check6(match, dict_easy_check)

				check1(match, hwivsh1, heroes_name, interesting_items)
				check2(match, dict_check2, interesting_items)
				check3(match, dict_check3, heroes_name)
				check4(match, dict_check4)
				check5(match, dict_check5 , interesting_items)
				check6(match, dict_check6 ,  interesting_items)
				check7(match, dict_check7 ,  interesting_items)
				check8(match, dict_check8)
				check9(match, dict_check9)
				check10(match, dict_check10)
				melee_team_check(match, dict_melee_team, heroes_groups)
				drow_wth_range_check(match, dict_drow_wth_range, heroes_groups)
				increase_dmg_team_check(match, dict_increse_dmg_team, heroes_groups)
				team_wthout_heal_check(match, dict_team_wthout_heal, heroes_groups)
				disable_heroes_in_team_check(match, dict_disable_heroes, heroes_groups)
				evassion_heroes_check(match, dict_evassion_heroes, heroes_groups)
				reduce_armor_team_check(match, dict_reduce_armor_team, heroes_groups)
				team_fight_rule_check(match, dict_team_fight_rule, heroes_groups)
				"""
				
				### Аналитика проэкций
				# Строим вектор турнира
				
				dict_vector = dict_vector_init()
				vector_check(match, dict_vector, heroes_groups)
				list_vector_minimized = vector_minimize(dict_vector)
				del(dict_vector)
				ctry_vector_counting(list_vector_minimized, dict_vector_check, match['radiant_win'] )
				

			## строка состояния: показывает количество обработанных турниров
			print("Run time: {:.3f} min |||".format(( time.time() - start_time)/60 ), "1kk time: {:.3f} min".format(( time.time() - start_time)/60*(1000000/counter)), "ids: ", counter, ' 111', end='\r')
			if counter % 10**3 == 0:
				print()
				print(cheked_mtch_cnt)
			"""## выводит размер словарей в Mb			
				for key in dict_pool:
					print(asizeof.asizeof(dict_pool[key]) / float(1024*1024))
			"""
			"""
				print(asizeof.asizeof(dict_easy_check) / float(1024*1024))
				print(asizeof.asizeof(hwivsh1) / float(1024*1024))
				print(asizeof.asizeof(dict_check2) / float(1024*1024))
				print(asizeof.asizeof(dict_check3) / float(1024*1024))
				print(asizeof.asizeof(dict_check4) / float(1024*1024))
				print(asizeof.asizeof(dict_check5) / float(1024*1024))
				print(asizeof.asizeof(dict_check6) / float(1024*1024))
				print(asizeof.asizeof(dict_check7) / float(1024*1024))
				print(asizeof.asizeof(dict_check8) / float(1024*1024))
				print(asizeof.asizeof(dict_check9) / float(1024*1024))
				print(asizeof.asizeof(dict_check10) / float(1024*1024))
				print(asizeof.asizeof(dict_melee_team) / float(1024*1024))
				print(asizeof.asizeof(dict_drow_wth_range) / float(1024*1024))
				print(asizeof.asizeof(dict_increse_dmg_team) / float(1024*1024))
				print(asizeof.asizeof(dict_team_wthout_heal) / float(1024*1024))
				print(asizeof.asizeof(dict_disable_heroes) / float(1024*1024))
				print(asizeof.asizeof(dict_evassion_heroes) / float(1024*1024))
				print(asizeof.asizeof(dict_reduce_armor_team) / float(1024*1024))
				print(asizeof.asizeof(dict_team_fight_rule) / float(1024*1024))
				"""


	print()
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_vector_check.pickle', 'wb') as f:
		pickle.dump( dict_vector_check, f)
	"""for key in dict_pool:
		with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_' + key +'.pickle', 'wb') as f:
			pickle.dump( dict_pool[key], f)
		print(key)
	"""
	"""
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_easy_check.pickle', 'wb') as f:
		pickle.dump( dict_easy_check, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check1.pickle', 'wb') as f:
		pickle.dump( hwivsh1, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check2.pickle', 'wb') as f:
		pickle.dump( dict_check2, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check3.pickle', 'wb') as f:
		pickle.dump( dict_check3, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check4.pickle', 'wb') as f:
		pickle.dump( dict_check4, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check5.pickle', 'wb') as f:
		pickle.dump( dict_check5, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check6.pickle', 'wb') as f:
		pickle.dump( dict_check6, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check7.pickle', 'wb') as f:
		pickle.dump( dict_check7, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check8.pickle', 'wb') as f:
		pickle.dump( dict_check8, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check9.pickle', 'wb') as f:
		pickle.dump( dict_check9, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_check10.pickle', 'wb') as f:
		pickle.dump( dict_check10, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_melee_team.pickle', 'wb') as f:
		pickle.dump( dict_melee_team, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_drow_wth_range.pickle', 'wb') as f:
		pickle.dump( dict_drow_wth_range, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_increse_dmg_team.pickle', 'wb') as f:
		pickle.dump( dict_increse_dmg_team, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_team_wthout_heal.pickle', 'wb') as f:
		pickle.dump( dict_team_wthout_heal, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_disable_heroes.pickle', 'wb') as f:
		pickle.dump( dict_disable_heroes, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_evassion_heroes.pickle', 'wb') as f:
		pickle.dump( dict_evassion_heroes, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_reduce_armor_team.pickle', 'wb') as f:
		pickle.dump( dict_reduce_armor_team, f)
	with open('/var/statsman/cheked/' + str(retantion // 10**6) + 'kk_dict_team_fight_rule.pickle', 'wb') as f:
		pickle.dump( dict_team_fight_rule, f)
	"""


def main():
	# hwi = finded_to_dict('/var/statsman/finded/rare_items.txt', hwi_update)
	# hwiVSh = finded_to_dict('/var/statsman/finded/item_vs_hero.txt', hwiVSh_update)
	# read_haystack_hwi('/var/optimus/7.haystack', hwi, hwiVSh, 1000000, delete = False)

	# h = finded_to_dict('/var/statsman/finded/hero_acc.txt', h_update)
	# read_haystack_hero('/var/optimus/7.haystack', h, 1000000)
	
	print('main')
	with open(load_json_file("heroes.json")) as heroes_json:
		heroes_name = json.load(heroes_json)['heroes']
	print('heroes')
	with open(load_json_file("items.json")) as items_json:
		items_name = json.load(items_json)['items']
	print('items')
	with open(load_json_file("heroes_groups.json")) as heroes_groups_json:
		heroes_groups = json.load(heroes_groups_json)['groups']
		print('heroes_groups')

	interesting_items = (1,11,26,27,29,30,37,
	  50,63,65,79,81,90,92,98,102,104,201,202,203,204,106,193,194,108,
	  110,112,114,116,119,121,123,125,127,133,135,137,139,141,143,145,147,149,151,152,
	  154,156,158,160,162,164,166,168,170,172,174,196,176,178,180, 187,190,206,208,210,
	  212,214,220,226,229,231,235,247,249,254,242,239,232,236)
	interesting_no_items = (1, 30, 48, 50, 63, 79, 81, 90, 98, 102, 104, 201, 202, 203, 204,
	  106, 193, 194, 108, 110, 112, 114, 116, 119, 121, 127, 133, 135, 137, 139, 141, 143,
	  145, 147, 149, 151, 152, 156, 154, 158, 160, 168, 172, 174, 196, 176, 180, 187, 190,
	  206, 208, 210, 214, 220, 226, 229, 231, 235, 247, 249, 254, 242, 232, 236, 250, 252, 263)

	int_items = {}
	# словарь с каунтерами для каждого предмета
	for i, item in enumerate(interesting_items):
		int_items[str(item)] = 0
	read_haystack_main('/var/optimus/7.haystack', 1000000, heroes_name, heroes_groups, interesting_items)
	# read_haystack_mem_check('/var/optimus/7.haystack', 1000000, heroes_name, interesting_items)
	# hhw = finded_to_dict('/var/statsman/finded/hero_with_hero_with_win_acc_1m.txt', hhw_update)
	# read_haystack_hhw('/var/optimus/7.haystack', hhw, 1000000)

if __name__ == "__main__":
	main()
