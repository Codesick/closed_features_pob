
import pickle
import numpy as np
from scipy import stats
from fitter import Fitter
import matplotlib.pyplot as plot

# fit func
def sign_fit(data):
	f = Fitter(data, timeout=60)
	f.fit()
	return f.get_best()

def make_readable_file(funcs, name):
	res_text = ''
	for key, value in funcs.items():
		res_text += str(key) + ':' + str(value) + '\n'
		res_text += '\n'
	with open('/var/statsman/heroes/' + name + '.txt','w') as f:
		f.write(res_text)

def hero_fitter_pool(key, value):
	func_wth_param = {}
	func_wth_param['duration'] = sign_fit(value['duration'][0:10000])
	func_wth_param['kills'] = sign_fit(value['kills'][0:10000])
	func_wth_param['deaths'] = sign_fit(value['deaths'][0:10000])
	func_wth_param['gold_per_min'] = sign_fit(value['gold_per_min'][0:10000])
	func_wth_param['hero_damage'] = sign_fit(value['hero_damage'][0:10000])
	func_wth_param['hero_healing'] = sign_fit(value['hero_healing'][0:10000])
	make_readable_file(func_wth_param, value['localized_name'])
	return value['localized_name']

# hists
def bins_create(data, bins):
	hist = np.histogram(data, bins = bins, density = True)
	# print(hist[0].size, hist[1].size)
	# print(hist[0][:10])
	# print(hist[1][:10])
	# h = plot.hist(data, bins = 10000)
	# print('painting')
	# plot.show()
	# with open('test_hist.pickle', 'wb') as f:
	# 	pickle.dump( hist, f)
	return hist

def make_pickle(dict_of_hists, name ):
	with open('/var/statsman/heroes_hist/' + name + '.pickle', 'wb') as f:
	# with open(name + '.pickle', 'wb') as f:	
		pickle.dump( dict_of_hists, f)



def hero_hist_pool(key, value):
	dict_of_hists = {}
	dict_of_hists['duration'] = bins_create(value['duration'],6600)
	dict_of_hists['kills'] = bins_create(value['kills'],50)
	dict_of_hists['deaths'] = bins_create(value['deaths'],30)
	dict_of_hists['gold_per_min'] = bins_create(value['gold_per_min'],400)
	dict_of_hists['hero_damage'] = bins_create(value['hero_damage'],20000)
	dict_of_hists['hero_healing'] = bins_create(value['hero_healing'],10000)
	# make_readable_file(func_wth_param, value['localized_name'])
	make_pickle(dict_of_hists, value['localized_name'] )
	return value['localized_name']

def main():
	counter = 0
	print('wait_pls')
	with open('/var/statsman/cheked/8m_dict_hero_stats_data.pickle', 'rb') as f:
	# with open('/Users/A/Documents/slava/Dropbox/other_scripts/8m_dict_hero_stats_data.pickle', 'rb') as f:
		dict_hero_stats_data = pickle.load(f)
	print('dict_loaded')

	for key, value in dict_hero_stats_data.items():
		if value['matches'] > 10000:
			print(hero_hist_pool(key, value))

if __name__ == "__main__":
	main()
