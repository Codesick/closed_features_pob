#time_heroes
def heroes_avg_game_time(hashs, heroes_name):
	avg_time = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		for i in range(0, 180):
			minute = str(i)

			try:
				rad = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_rad']['h'][hero],
												hashs['heroes_rad']['m'][hero]
												) 
				dire = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_dire']['h'][hero],
												hashs['heroes_dire']['m'][hero]
												) 
				match_cnt += rad + dire
				time_cnt += i * (rad + dire)

			except Exception:
				raise

		avg_time[hero] = time_cnt / float(match_cnt)




#time_heroes_win
def heroes_avg_win_game_time(hashs, heroes_name):
	avg_time = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		for i in range(0, 180):
			minute = str(i)

			try:
				rad = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_win_rad']['h'][hero],
												hashs['heroes_win_rad']['m'][hero]
												) 
				dire = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_win_dire']['h'][hero],
												hashs['heroes_win_dire']['m'][hero]
												) 
				match_cnt += rad + dire
				time_cnt += i * (rad + dire)

			except Exception:
				raise

		avg_time[hero] = time_cnt / float(match_cnt)


#time_heroes_lose
def heroes_avg_lose_game_time(hashs, heroes_name):
	avg_time = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		for i in range(0, 180):
			minute = str(i)

			try:
				rad = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_lose_rad']['h'][hero],
												hashs['heroes_lose_rad']['m'][hero]
												) 
				dire = intersect_and_count(hashs['time']['h'][minute],
												hashs['time']['m'][minute],
												hashs['heroes_lose_dire']['h'][hero],
												hashs['heroes_lose_dire']['m'][hero]
												) 
				match_cnt += rad + dire
				time_cnt += i * (rad + dire)

			except Exception:
				raise

		avg_time[hero] = time_cnt / float(match_cnt)


#time_heroes_damage
def heroes_avg_dmg_per_min(hashs, heroes_name):
	avg_dpmin = {}
	avg_dpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		dmg_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			dmg_cnt_m = 0
			for j in range(0,300):
				h_dmg = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_damage_rad']['h'][h_dmg],
													hashs['heroes_damage_rad']['m'][h_dmg]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_damage_dire']['h'][h_dmg],
													hashs['heroes_damage_dire']['m'][h_dmg]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					dmg_cnt_m += j * 500 * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_dpmin[hero + '_' + minute] = dmg_cnt_m / float(time_cnt_m)
				avg_dpmatch[hero + '_' + minute] = dmg_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			dmg_cnt +=dmg_cnt_m
		avg_dpmin[hero] = dmg_cnt / float(time_cnt)
		avg_dpmatch[hero] = dmg_cnt / float(match_cnt)


#time_tower_damage
def heroes_avg_td_per_min(hashs, heroes_name):
	avg_tdpmin = {}
	avg_tdpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		td_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			td_cnt_m = 0
			for j in range(0,70):
				h_td = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_td_rad']['h'][h_td],
													hashs['heroes_td_rad']['m'][h_td]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_td_dire']['h'][h_td],
													hashs['heroes_td_dire']['m'][h_td]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					td_cnt_m += j * 200 * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_tdpmin[hero + '_' + minute] = td_cnt_m / float(time_cnt_m)
				avg_tdpmatch[hero + '_' + minute] = td_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			td_cnt +=td_cnt_m
		avg_tdpmin[hero] = td_cnt / float(time_cnt)
		avg_tdpmatch[hero] = td_cnt / float(match_cnt)


#time_gpm
def heroes_avg_gpm(hashs, heroes_name):
	avg_gpmpmin = {}
	avg_gpmpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		gpm_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			gpm_cnt_m = 0
			for j in range(0,40):
				h_gpm = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_gpm_rad']['h'][h_gpm],
													hashs['heroes_gpm_rad']['m'][h_gpm]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_gpm_dire']['h'][h_gpm],
													hashs['heroes_gpm_dire']['m'][h_gpm]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					gpm_cnt_m += j * 30 * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_gpmpmin[hero + '_' + minute] = gpm_cnt_m / float(time_cnt_m)
				avg_gpmpmatch[hero + '_' + minute] = gpm_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			gpm_cnt +=gpm_cnt_m
		avg_gpmpmin[hero] = gpm_cnt / float(time_cnt)
		avg_gpmpmatch[hero] = gpm_cnt / float(match_cnt)


#time_last_hits
def heroes_avg_lh(hashs, heroes_name):
	avg_lhpmin = {}
	avg_lhpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		lh_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			lh_cnt_m = 0
			for j in range(0,100):
				h_lh = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_lh_rad']['h'][h_lh],
													hashs['heroes_lh_rad']['m'][h_lh]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_lh_dire']['h'][h_lh],
													hashs['heroes_lh_dire']['m'][h_lh]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					lh_cnt_m += j * 20 * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_lhpmin[hero + '_' + minute] = lh_cnt_m / float(time_cnt_m)
				avg_lhpmatch[hero + '_' + minute] = lh_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			lh_cnt +=lh_cnt_m
		avg_lhpmin[hero] = lh_cnt / float(time_cnt)
		avg_lhpmatch[hero] = lh_cnt / float(match_cnt)


#time_hero_value
def heroes_avg_value(hashs, heroes_name):
	avg_valuepmin = {}
	avg_valuepmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		value_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			value_cnt_m = 0
			for j in range(0,200):
				h_value = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_gold_rad']['h'][h_value],
													hashs['heroes_gold_rad']['m'][h_value]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_gold_dire']['h'][h_value],
													hashs['heroes_gold_dire']['m'][h_value]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					value_cnt_m += j * 500 * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_valuepmin[hero + '_' + minute] = value_cnt_m / float(time_cnt_m)
				avg_valuepmatch[hero + '_' + minute] = value_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			value_cnt +=value_cnt_m
		avg_valuepmin[hero] = value_cnt / float(time_cnt)
		avg_valuepmatch[hero] = value_cnt / float(match_cnt)


#time_last_kills
def heroes_avg_kill(hashs, heroes_name):
	avg_killpmin = {}
	avg_killpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		kill_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			kill_cnt_m = 0
			for j in range(0,100):
				h_kill = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_kill_rad']['h'][h_kill],
													hashs['heroes_kill_rad']['m'][h_kill]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_kill_dire']['h'][h_kill],
													hashs['heroes_kill_dire']['m'][h_kill]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					kill_cnt_m += j * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_killpmin[hero + '_' + minute] = kill_cnt_m / float(time_cnt_m)
				avg_killpmatch[hero + '_' + minute] = kill_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			kill_cnt +=kill_cnt_m
		avg_killpmin[hero] = kill_cnt / float(time_cnt)
		avg_killpmatch[hero] = kill_cnt / float(match_cnt)


#time_last_death
def heroes_avg_kill(hashs, heroes_name):
	avg_killpmin = {}
	avg_killpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		kill_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			kill_cnt_m = 0
			for j in range(0,100):
				h_kill = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_kill_rad']['h'][h_kill],
													hashs['heroes_kill_rad']['m'][h_kill]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_kill_dire']['h'][h_kill],
													hashs['heroes_kill_dire']['m'][h_kill]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					kill_cnt_m += j * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_killpmin[hero + '_' + minute] = kill_cnt_m / float(time_cnt_m)
				avg_killpmatch[hero + '_' + minute] = kill_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			kill_cnt +=kill_cnt_m
		avg_killpmin[hero] = kill_cnt / float(time_cnt)
		avg_killpmatch[hero] = kill_cnt / float(match_cnt)

#contr_armor_reduce
def contr_armor_reduce(hashs, heroes_name, armor_reduce_item, contr_ar_items, armor_reduce_heroes, contr_ar_heroes):

	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		for ii, item in enumerate(armor_reduce_item):
			hash_name = hero + '_' + item
			rad = 0
			dire = 0
			for iii, contr_hero in enumerate(contr_ar_heroes):
				try:
					rad +=  intersect_and_count(hashs['heroes_item_rad']['h'][hash_name],
												hashs['heroes_dire']['h'][contr_hero])
					dire += intersect_and_count(hashs['heroes_item_dire']['h'][hash_name],
												hashs['heroes_rad']['h'][contr_hero])
				except Exception:
					pass
			for iii, contr_item in enumerate(contr_ar_heroes):
				for iiii, allies_hero in enumerate(heroes_name):
					contr_hash_name = str(allies_hero['id']) + '_' + contr_item
					try:
						rad +=  intersect_and_count(hashs['heroes_item_rad']['h'][hash_name],
													hashs['heroes_item_dire']['h'][contr_name])
						dire += intersect_and_count(hashs['heroes_item_dire']['h'][hash_name],
													hashs['heroes_item_rad']['h'][contr_name])
					except Exception:
						pass



#time_last_assiste
def heroes_avg_assist(hashs, heroes_name):
	avg_assistpmin = {}
	avg_assistpmatch = {}
	for i, heroes in enumerate(heroes_name):
		hero = str(heroes['id'])
		match_cnt = 0
		time_cnt = 0
		assist_cnt = 0
		for i in range(0, 180):
			minute = str(i)
			match_cnt_m = 0
			time_cnt_m = 0
			assist_cnt_m = 0
			for j in range(0,120):
				h_assist = hero + '_' + str(j)
				try:
					rad = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_assist_rad']['h'][h_assist],
													hashs['heroes_assist_rad']['m'][h_assist]
													) 
					dire = intersect_and_count(hashs['time']['h'][minute],
													hashs['time']['m'][minute],
													hashs['heroes_assist_dire']['h'][h_assist],
													hashs['heroes_assist_dire']['m'][h_assist]
													) 
					match_cnt_m += rad + dire
					time_cnt_m += i * (rad + dire)
					assist_cnt_m += j * (rad + dire)
				except Exception:
					raise
			if match_cnt_m != 0:
				avg_assistpmin[hero + '_' + minute] = assist_cnt_m / float(time_cnt_m)
				avg_assistpmatch[hero + '_' + minute] = assist_cnt_m / float(match_cnt_m)
			match_cnt += match_cnt_m
			time_cnt += time_cnt_m
			assist_cnt +=assist_cnt_m
		avg_assistpmin[hero] = assist_cnt / float(time_cnt)
		avg_assistpmatch[hero] = assist_cnt / float(match_cnt)



